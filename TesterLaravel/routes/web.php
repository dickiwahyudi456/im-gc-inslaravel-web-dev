<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/home', function () {
//     return "wadaw";



// });

Route::get('/','HomeController@index');
Route::get('/register','AuthController@v_register');
Route::get('/welcome','AuthController@v_welcome');
Route::get('/table','table@index');
Route::get('/data-tables','datatables@index');
// Route::get('/data-tables','data-tables@index');
